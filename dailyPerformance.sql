select
    -- *
    CONVERT(varchar, lp.CreatedDate, 23)  as 'CreatedDate',
    sum(case when lp.Status = 'Approved' then 1 else 0 end) as 'LoansIssued',
    sum(case when lp.Status = 'Approved' then la.LoanAmount else 0 end) as 'CashIssued',
    sum(case when lp.Status = 'Approved' then la.LoanAmount else 0 end) as 'CapitalIssued',
    sum(case when lp.Status = 'Deactivated' and la.LoanStatus='Settled' and convert(varchar, lp.CreatedDate, 108) <= '21:20' then la.TotalOutstandingBalance else 0 end) as 'TotalCollected',
    --sum(case when lp.Status in ('Due', 'Approved') then 1 else 0 end) as 'MTDLoansinOPB',
    --sum(case when lp.Status = 'Due' then 1 else 0 end) as 'MTDLoansinOPB',
    sum(case when lp.CreatedDate > DATEADD(DAY, DATEDIFF(DAY, 1, lp.CreatedDate), 0) and lp.Status in ('Due', 'Approved') then 1 else 0 end) as 'testMTDLoansinOPB',
    --sum(case when lp.Status in ('Due', 'Approved') then la.TotalOutstandingPrincipal else 0 end) as 'MTDValueinOPB',
    sum(case when lp.CreatedDate > DATEADD(DAY, DATEDIFF(DAY, 1, lp.CreatedDate), 0) and lp.Status in ('Due', 'Approved') then la.TotalOutstandingPrincipal else 0 end) as 'testMTDValueinOPB'
--sum(case when )
--sum(DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate)) as 'DaysInArears',
--sum(case when lp.Status = 'Due' then 1 end) as 'CountinArrears',
--sum(case when lp.Status = 'Due' then DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate) else 0 end)/sum(case when lp.Status = 'Due' then 1 end) as 'Arrears Days',
--case when AVG(DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate))<0 then 0 else AVG(DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate)) end as 'AvgDaysInArears',
--AVG(DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate)) OVER (ORDER by lp.CreatedDate DESC ROWS 10 PRECEDING) as 'AvgDaysInArears2',
--sum(case when lp.Status = 'Due' then la.TotalOutstandingBalance end)/sum(case when lp.Status = 'Due' then 1 end) as 'AvgArrearsperLoan'
-- AVG(case when lp.Status = 'Due' then DATEDIFF(day, la.FirstRepaymentDate, lp.CreatedDate) end) as 'DaysInArears'
from
    WageX.dbo.LoanAccounts la
    join
    WageX.dbo.LoanProcesses lp
    on
la.Id = lp.LoanAccountId
where lp.CreatedDate <= GETDATE()
group by lp.CreatedDate, la.FirstRepaymentDate, lp.Status
order by lp.CreatedDate desc
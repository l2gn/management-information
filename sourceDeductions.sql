
-- Employers and customers disbursed by current year and month

select
  EmployerName as 'Employer Name'
, CreatedDate as 'Disbursement Date'
, LoanAmount as ' Amount Disbursed'
, Installment-LoanAmount as 'Fee Due' 
, Installment as 'Total Amount Due'
, TotalOutstandingInterest * 0.20 as 'Commission Due Employer'
from Wagex..LoanAccounts
where LoanStatus='Approved'
    and month(CreatedDate) = 2
    and year(CreatedDate) = 2021
group by EmployerName, CreatedDate, LoanAmount, Installment, TotalOutstandingInterest
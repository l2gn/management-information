-- total count today
select count(*) from WageX.dbo.Customers au
--where convert(varchar, au.CreatedDate, 23) = convert(varchar, GETDATE(), 23)
where convert(varchar, au.CreatedDate, 23)>= '2021-02-12'
--
--where day(convert(varchar, au.CreatedDate, 23))>= 12
--and month(convert(varchar, au.CreatedDate, 23)) >= 2
--where year(convert(varchar, au.CreatedDate, 23))>= 2021



-- get all customer's with other as company
select 
ar.LastName
, ar.FirstName
, ar.PhoneNumber1
, ar.PhoneNumber2
, CONVERT(varchar, ar.CreatedDate, 23)
, ar.PreferredPhoneNumber 
, ar.Email 
from Wagex.dbo.Customers ar
where CompanyId = 10 -- CompanyId is NULL -- or 
and LastName is not NULL 
--and LastName not like 'ODEN'
--and convert(varchar, ar.CreatedDate, 23) >= '2021-02-11'


-- get count of customer's in companies
select 
c2.Name as companyName
, count(*) as applicationVolume
, sum(case when c.IsVerified = 1 then 1 else 0 end) as verifiedVolume
from WageX.dbo.Customers c 
left join WageX.dbo.Company c2 
on c.CompanyId =c2.Id 
--where c2.Name not NULL
--where convert(varchar, c.CreatedDate, 23) = '2021-02-18'
group by c2.Name 
;

select * from WageX.dbo.Company c 
where Id = 12

select * from WageX.dbo.Customers c2 
--where c2.CompanyId = 12
order by c2.CreatedDate desc



select 
c.FirstName
,c.LastName
,c.PhoneNumber1 
--,c.PhoneNumber2 
,c.Email 
,c2.Name 
,convert(varchar, c.CreatedDate, 23)
from WageX.dbo.Customers c 
left join WageX.dbo.Company c2 
on c.CompanyId =c2.Id 
where c.IsVerified = 1
--c2.Name = 'OTHERS'
order by convert(varchar, c.CreatedDate, 23) desc

-- get customer's in Loans2Go
select 
c.*
,c2.Name as companyName
--, count(*) as numbers
from WageX.dbo.Customers c 
join WageX.dbo.Company c2 
on c.CompanyId =c2.Id 
where c2.Name like 'LONGBRIDGE TECHNOLOGIES LIMITED%'
--group by c2.Name 


select * from WageX.dbo.Company c 

select
c.id
,c.CreatedDate 
,c.ModifiedBy 
,c.UserName 
,c.LastName 
,c.FirstName 
,c.PhoneNumber1 
,c.DateOfBirth 
,c.Gender 
,c.MaritalStatus 
,CONCAT(c.HouseNumber, ', ', c.StreetName, ', ', c.City) 
,c.MonthlyIncome 
,c.IsSalaryOnRemita
,la.MonthlyIncome 
from WageX.dbo.Customers c 
join WageX.dbo.LoanAccounts la 
on Id =la.CustomerId 
where CompanyId = 11
and id = 330


select * from WageX.dbo.LoanAccounts la
order by CreatedDate DESC 

select Id, CreditScore, CreditRating from WageX.dbo.Customers c
where CreditScore is not null
order by CreatedDate DESC 

select * from WageX.dbo.CollectionTransaction ct 


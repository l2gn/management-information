select 

--la.LoanAccountId
la.CompnayLoanCode
,la.EmployerName
--,PARSENAME(REPLACE(la.AccountName,' ','.'),2) 'Name'
--,PARSENAME(REPLACE(la.AccountName,' ','.'),1) 'Surname'
--,case when lp.Active=1 then 'Active' else 'Completed' end as 'Loan State'
,la.LoanAmount
,la.Tenor
--,cast(Month(la.FirstRepaymentDate), '-', Day(la.FirstRepaymentDate)) as 'Payment Date'

from 
WageX.dbo.LoanAccounts la 
join
WageX.dbo.LoanProcesses lp 
on
la.Id = lp.LoanAccountId 
group by la.CompnayLoanCode ,la.EmployerName,la.LoanAmount,la.Tenor



--group by la.CompnayLoanCode ,la.EmployerName,PARSENAME(REPLACE(la.AccountName,' ','.'),2),PARSENAME(REPLACE(la.AccountName,' ','.'),1),case when lp.Active=1 then 'Active' else 'Completed' end




--where lp.CreatedDate <= GETDATE() 
--group by lp.CreatedDate, la.FirstRepaymentDate, lp.Status 
--order by lp.CreatedDate desc
;

select
lp.LoanAccountId
,convert(varchar, lp.CreatedDate, 23) as Date
,convert(varchar, lp.CreatedDate, 108) as Time
,case when lp.Status = 'Deactivated' then la.TotalOutstandingBalance
	  when lp.Status = 'Approved' then la.LoanAmount
	  End as 'Amount'
,case when lp.Status = 'Deactivated' then 'Collection'
	  when lp.Status = 'Approved' then 'Disbursement'
	  END as 'TransactionType'
,case when lp.Status in ('Deactivated', 'Approved') then 'Direct Debit' end as 'Method'
,la.EmployerName as 'User'
,la.EmployerContactNumber as 'EmployerDetails'
,la.DisburseAccountName as 'AccountName'
,la.DisburseAccountNumber as 'AccountNumber'
,la.LoanAmount as 'RequestAmount'
-- ,case when Day(lp.CreatedDate) > Day(la.FirstRepaymentDate) then 'Late' else 'Early' end as 'PaidTime'

	  
select
*
from
	(
		select
			la.Id 
			,la.CompnayLoanCode as 'Agr. No'
			, la.EmployerName as 'Employer Name'
			, c.FirstName as 'Name'
			, c.LastName as 'Surname'
			, case
				when la.LoanStatus = 'Settled' then 'Completed'
				when la.LoanStatus = 'Approved' then 'Active'
				end as 'Loan State'
			--, case when 
			, cast(
				round(la.LoanAmount, 2, 0) as decimal(18, 2)
					) as 'Loan Amount'
			, la.Tenor as 'Term'
			, la.SalaryFrequency as 'Payment Frequency'
			, cast(
				round(la.TotalOutstandingPrincipal + la.TotalOutstandingInterest, 2, 0) as decimal(18, 2)
					) as 'TAP'
			, CONVERT(varchar, la.CreatedDate, 23) as 'Signed Date'
			, case when ((la.TotalOutstandingPrincipal + la.TotalOutstandingInterest)- la.TotalOutstandingBalance) + (la.TotalOutstandingBalance -la.LoanAmount)>0 then 0
				else ((la.TotalOutstandingPrincipal + la.TotalOutstandingInterest)- la.TotalOutstandingBalance) + (la.TotalOutstandingBalance -la.LoanAmount) end as 'Balance'
			, la.TotalOutstandingBalance -la.LoanAmount as 'Ttl.chg'
			, case when la.LoanStatus ='Settled' then la.TotalOutstandingBalance else 0 end as 'Ttl.pmt'
			--, la.TotalOutstandingBalance as 'Ttl.pmt'
			, la.Installment as 'Installment Amount'
			, case when la.LoanStatus ='Settled' then convert(varchar, la.ModifiedDate, 23) end as 'Last Payment Date'
			, case when la.LoanStatus ='Settled' then la.TotalOutstandingBalance else 0 end as 'Total Recieved MTD'
			, convert(varchar, la.FirstRepaymentDate, 23) as 'First Payment Due'
			--,DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),getdate()) AS 'Days Past Due'
			, case when la.LoanStatus = 'Settled' and la.ModifiedDate <= la.FirstRepaymentDate then 0
				   when la.LoanStatus = 'Settled' and la.ModifiedDate > la.FirstRepaymentDate then DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),la.ModifiedDate)
				   when la.LoanStatus = 'Approved' and la.ModifiedDate > la.FirstRepaymentDate then DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),la.ModifiedDate)
				   when la.LoanStatus = 'Approved' and la.ModifiedDate <= la.FirstRepaymentDate then 0
				   end as 'Days Past Due'
 			from WageX.dbo.Customers c
			join WageX.dbo.LoanAccounts la on
			c.Id = la.CustomerId
			--inner join WageX.dbo.LoanProcesses lp 
			--on la.Id = lp.LoanAccountId

			where
			case
				when la.LoanStatus = 'Settled' then 'Completed'
				when la.LoanStatus = 'Approved' then 'Active'
			end != 'NULL'
			) as x
			INNER JOIN (
				select 
					lp.LoanAccountId
					,convert(varchar, MAX(lp.CreatedDate), 23) as 'Last State Change'
					--,lp.Status 
--					,case when MAX(lp.CreatedDate) = lp.CreatedDate then lp.Status end as 'State at First'
					from WageX.dbo.LoanProcesses lp 
					where lp.Status not in ('Originated', 'Rejected')
					group by LoanAccountId
				) as b
			on x.Id=b.LoanAccountId


select * from WageX.dbo.LoanAccounts la 

select * from WageX.dbo.Customers c 

select * from WageX.dbo.LoanProcesses lp  


SELECT TOP (1) lp.id, lp.CreatedDate 
FROM WageX.dbo.LoanProcesses lp 
WHERE lp.CreatedDate < ( SELECT MAX(lp1.CreatedDate) 
               FROM WageX.dbo.LoanProcesses lp1
             ) 
group by lp.Id, lp.CreatedDate
ORDER BY lp.CreatedDate DESC ;

select * from WageX.dbo.CollectionTransaction ct


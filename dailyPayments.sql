-- Daily Payments Receipts
select
    --lp.LoanAccountId
    la.CompnayLoanCode as 'Agr. No'
, convert(varchar, lp.CreatedDate, 23) as Date
, convert(varchar, lp.CreatedDate, 108) as Time
, case when lp.Status = 'Deactivated' then la.TotalOutstandingBalance
	  when lp.Status = 'Approved' then la.LoanAmount
	  End as 'Amount'
, case when lp.Status = 'Deactivated' then 'Collection'
	  when lp.Status = 'Approved' then 'Disbursement'
	  END as 'TransactionType'
, case when lp.Status in ('Deactivated', 'Approved') then 'Direct Debit' end as 'Method'
, la.EmployerName as 'User'
, la.EmployerContactNumber as 'EmployerDetails'
, la.DisburseAccountName as 'AccountName'
, la.DisburseAccountNumber as 'AccountNumber'
, la.LoanAmount as 'RequestAmount'
-- ,case when Day(lp.CreatedDate) > Day(la.FirstRepaymentDate) then 'Late' else 'Early' end as 'PaidTime'

from
    WageX.dbo.LoanAccounts la
    join
    WageX.dbo.LoanProcesses lp
    on
la.Id = lp.LoanAccountId
where lp.CreatedDate <= GETDATE()
    and case when lp.Status = 'Deactivated' then 'Collection'
	  when lp.Status = 'Approved' then 'Disbursement'
	  END  is not NULL
group by la.CompnayLoanCode, lp.Status, lp.CreatedDate, la.LoanAmount , 
		 la.TotalOutstandingBalance, la.EmployerName, la.EmployerContactNumber,
		 la.DisburseAccountName, la.DisburseAccountNumber
order by lp.CreatedDate desc

;

select *
from WageX.dbo.ApplicationUsers au
use WageX
drop table if exists WageX.dbo.tmpgl
CREATE TABLE Wagex.dbo.tmpgl
(
--    ProductName VARCHAR(100),
--    CostCentre VARCHAR(20),
--    --BranchId VARCHAR(20),
--    Account VARCHAR(20),
--    [Date] datetime,
--    ActivityId VARCHAR(20),
--    --Amount VARCHAR(20),
--    GLAccount VARCHAR(20),
--    GLDescription VARCHAR(100),
--    --OOB VARCHAR(20),
--    TYNType VARCHAR(100),
--    TXNTypeId VARCHAR(20),
--    UploadDate datetime,
--    --[Rollup] VARCHAR(20)
  
    ProductName varchar(100)
	,CostCentre varchar(100)
	,Account varchar(100)
	,[Date] datetime
	,ActivityId varchar(100)
	,Amount varchar(100)
	,GLAccount varchar(100)
	,GLDescription varchar(100)
	,TXNType varchar(100)
	,TXNTypeId varchar(100)
	,UploadDate datetime
    
    
)


;

insert into #tmpgl 
select  
'WageX' as ProductName
,100 as CostCentre
--,'' as BranchId
--,la.CompnayLoanCode as Account
, la.AccountNumber as Account 
, convert(varchar, la.CreatedDate, 23) as [Date]
,'' as ActivityId
, case when lp.Status = 'Approved' then la.LoanAmount
	   --when lp.status = 'Approved' then la.TotalOutstandingInterest
	   when lp.Status ='Deactivated' then -(la.TotalOutstandingBalance -la.TotalOutstandingInterest)
	   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then -(la.TotalOutstandingBalance- (la.TotalOutstandingInterest +la.TotalOutstandingPrincipal))
	   end as Amount
--, '' as GLAccount
, case when lp.Status='Approved' then 129100
	   when lp.Status='Deactivated' then 129100
	   --when lp.Status='Approved' and lp.Comment ='Approved' then 609300
	   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 129300
	   end as GLAccount
, case when lp.Status='Approved' then 'PrincipalOriginations'
	   when lp.Status='Deactivated' then 'PrincipalOriginations'
	   --when lp.Status='Approved' and lp.Comment ='Approved' then 'InterestOriginations'
	   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 'Fees Originations'
	   end as GLDescription
--	   end as 'GLDescription'
--, 'N' as OOB
, case when lp.Status = 'Deactivated' then 'Account Moved to Completed State'
	   when lp.Status = 'Approved' then 'New Loans'
	   --when lp.Status = 'Settled' then 'Payments Received, Non written off accounts'
	   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 'Payments Received, Non written off accounts'
	   end as TXNType	   
, case when lp.Status = 'Deactivated' then 5
	   when lp.Status = 'Approved' then 2
	   --when lp.Status = 'Settled' then 'Payments Received, Non written off accounts'
	   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 1
	   end as TXNTypeId
--, '' as TXNTypeId
, convert(varchar, GETDATE(), 23) as UploadDate
--, 'Y' as 'Rollup'
from WageX.dbo.Customers c 
join WageX.dbo.LoanAccounts la 
on c.Id =la.CustomerId 
join WageX.dbo.LoanProcesses lp 
on la.Id = lp.LoanAccountId 
--group by case when lp.Status = 'Deactivated' then 'Account Moved to Completed State'
--	   		 when lp.Status = 'Approved' then 'New Loans'
--	  		 when lp.Status = 'Settled' then 'Payments Received, Non written off accounts'
--	  		 end,
--	  	 case when lp.Status = 'Approved' then 'Principal Originations'
--	   		  when lp.Status = 'Approved' then 'Interst Originations'
--	          end,
group by la.AccountNumber
		, case when lp.Status='Approved' then 129100
	   		   when lp.Status='Deactivated' then 129100
	   --when lp.Status='Approved' and lp.Comment ='Approved' then 609300
	           when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 129300
	           end
	   , convert(varchar, la.CreatedDate, 23)
	   , case when lp.Status='Approved' then 'PrincipalOriginations'
	   		  when lp.Status='Deactivated' then 'PrincipalOriginations'
	   --when lp.Status='Approved' and lp.Comment ='Approved' then 'InterestOriginations'
	   		  when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 'Fees Originations'
	   		  end
	   , case when lp.Status = 'Approved' then la.LoanAmount
	   --when lp.status = 'Approved' then la.TotalOutstandingInterest
	   		  when lp.Status ='Deactivated' then -(la.TotalOutstandingBalance -la.TotalOutstandingInterest)
	   		  when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then -(la.TotalOutstandingBalance- (la.TotalOutstandingInterest +la.TotalOutstandingPrincipal))
	          end
	   , case when lp.Status = 'Deactivated' then 'Account Moved to Completed State'
	   		  when lp.Status = 'Approved' then 'New Loans'
	   --when lp.Status = 'Settled' then 'Payments Received, Non written off accounts'
	   		  when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 'Payments Received, Non written off accounts'
	   		  end 
	   , case when lp.Status = 'Deactivated' then 5
			   when lp.Status = 'Approved' then 2
			   --when lp.Status = 'Settled' then 'Payments Received, Non written off accounts'
			   when lp.Status ='Deactivated' and la.TotalOutstandingBalance >la.TotalOutstandingPrincipal then 1
			   end
order by convert(varchar, la.CreatedDate, 23)


select * from #tmpgl
where Amount is not NULL 
order by Date DESC 
DECLARE @DATE DATETIME
SET @DATE=GETDATE()
--SELECT @DATE AS GIVEN_DATE, day(@DATE-DAY(@DATE)+1) AS FIRST_DAY_OF_DATE, 
--EOMONTH(@DATE) AS LAST_DAY_OF_MONTH

select
    x.AgrNo
, x.Branch
, x.Name
, x.Surname
, x.LoanState
, x.StateAt1st
, x.LoanCode
, x.LoanAmount
, x.Term
, x.PaymentFrequency
, x.TAP
, x.SignedDate
, x.Funded
, x.Balance
, x.TtlChg
, x.TtlPmt
, x.InstallmentAmount
, x.LastPaymentDate
, x.LastPaymentAmount
, x.TotalRecievedMTD
, x.CurrentArrears
, x.DaysInArrears
, x.DateOfWriteOff
, b.LastStateChangeDate
, x.FirstPaymentDue
, x.OPB
, x.SigningUW
, x.DaysPastDue
, x.TotalPMTexclNonPMTCredits
, x.TotalNonPMTcredits
, x.Subcode
from
    (
		select
        la.Id as ID
			, la.CompnayLoanCode as AgrNo
			, la.EmployerName as Branch
			--, '' as Branch
			, c.FirstName as Name
			, c.LastName as Surname
			, case
				when la.LoanStatus = 'Settled' then 'Completed'
				when la.LoanStatus = 'Approved' then 'Active'
				when la.LoanStatus = 'Due' then 'Due'
				end as LoanState
			, case when day(@DATE-DAY(@DATE)+1) = 1 and la.LoanStatus ='Settled' then 'Completed'
					when day(@DATE-DAY(@DATE)+1) = 1 and la.LoanStatus ='Approved' then 'Active'
					when day(@DATE-DAY(@DATE)+1) = 1 and la.LoanStatus ='Due' then 'Due' 
					end as StateAt1st
			, '' as LoanCode
			--, case when 
			, cast(
				round(la.LoanAmount, 2, 0) as decimal(18, 2)
					) as LoanAmount
			, la.Tenor as Term
			, la.SalaryFrequency as PaymentFrequency
			, cast(
				round(la.TotalOutstandingPrincipal + la.TotalOutstandingInterest, 2, 0) as decimal(18, 2)
					) as TAP
			, CONVERT(varchar, la.CreatedDate, 23) as SignedDate
			, '' as Funded
--			, case when ((la.TotalOutstandingPrincipal + la.TotalOutstandingInterest)- la.TotalOutstandingBalance) + (la.TotalOutstandingBalance -la.LoanAmount)>0 then 0
--				else ((la.TotalOutstandingPrincipal + la.TotalOutstandingInterest)- la.TotalOutstandingBalance) + (la.TotalOutstandingBalance -la.LoanAmount) end as 'Balance'
			, case when la.LoanStatus = 'Settled' then 0
				   when la.LoanStatus in ('Approved','Due') then la.TotalOutstandingPrincipal+la.TotalOutstandingInterest 
				   end as Balance
			, la.TotalOutstandingBalance -la.Installment as TtlChg
			, case when la.LoanStatus ='Settled' then la.TotalOutstandingBalance else 0 end as TtlPmt
			--, la.TotalOutstandingBalance as 'Ttl.pmt'
			, la.Installment as InstallmentAmount
			, case when la.LoanStatus ='Settled' then convert(varchar, la.ModifiedDate, 23) end as LastPaymentDate
			, case when la.LoanStatus ='Settled' and la.ModifiedDate >= la.FirstRepaymentDate then la.TotalOutstandingBalance
				   when la.LoanStatus ='Settled' and Month(la.ModifiedDate) >= Month(la.FirstRepaymentDate) then la.TotalOutstandingBalance
				   else 0 end as LastPaymentAmount
			, case when la.LoanStatus ='Settled' and la.ModifiedDate >= la.FirstRepaymentDate then la.TotalOutstandingBalance
				   when la.LoanStatus ='Settled' and Month(la.ModifiedDate) >= Month(la.FirstRepaymentDate) then la.TotalOutstandingBalance
				   else 0 end as TotalRecievedMTD
			, '' as CurrentArrears
			, '' as DaysInArrears
			, '' as DateOfWriteOff
			--, 

			, convert(varchar, la.FirstRepaymentDate, 23) as FirstPaymentDue
			, case when la.LoanStatus in ('Approved', 'Due') then la.TotalOutstandingPrincipal else 0 end as OPB --when la.LoanStatus ='Settled' then 0
			, '' as SigningUW
			--,DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),getdate()) AS 'Days Past Due'
			, case when la.LoanStatus = 'Settled' and la.ModifiedDate <= la.FirstRepaymentDate then 0
				   when la.LoanStatus = 'Settled' and la.ModifiedDate > la.FirstRepaymentDate then DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),la.ModifiedDate)
				   when la.LoanStatus = 'Approved' and la.ModifiedDate > la.FirstRepaymentDate then DATEDIFF(day, convert(varchar, la.FirstRepaymentDate, 23),la.ModifiedDate)
				   when la.LoanStatus = 'Approved' and la.ModifiedDate <= la.FirstRepaymentDate then 0
				   end as DaysPastDue
			, '' as TotalPMTexclNonPMTCredits
			, '' as TotalNonPMTcredits
			, '' as Subcode
    from WageX.dbo.Customers c
        join WageX.dbo.LoanAccounts la on
			c.Id = la.CustomerId
    --inner join WageX.dbo.LoanProcesses lp 
    --on la.Id = lp.LoanAccountId

    where
			case
				when la.LoanStatus = 'Settled' then 'Completed'
				when la.LoanStatus = 'Approved' then 'Active'
			end != 'NULL'
			) as x
    INNER JOIN (
				select
        lp.LoanAccountId as 'ID'
					, convert(varchar, MAX(lp.CreatedDate), 23) as LastStateChangeDate
    --,lp.Status 
    --					,case when MAX(lp.CreatedDate) = lp.CreatedDate then lp.Status end as 'State at First'
    from WageX.dbo.LoanProcesses lp
    where lp.Status not in ('Originated', 'Rejected')
    group by LoanAccountId
				) as b
    --using(b.ID)
    on x.ID=b.ID
			--using(b.LoanAccountId)
			--where x.Id=b.LoanAccountId